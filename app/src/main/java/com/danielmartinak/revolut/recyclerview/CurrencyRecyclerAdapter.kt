package com.danielmartinak.revolut.recyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.danielmartinak.revolut.R
import kotlinx.android.synthetic.main.rate_card_item.view.*
import java.util.*

class CurrencyRecyclerAdapter(
    var rates: List<Pair<String, Double>>,
    private val clickListener: (String, Double) -> Unit
) : RecyclerView.Adapter<CurrencyRecyclerAdapter.CurrencyViewHolder>() {

    var baseAmount: Double = 1.0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val inflatedView =
            LayoutInflater.from(parent.context).inflate(R.layout.rate_card_item, parent, false)
        return CurrencyViewHolder(inflatedView)
    }

    override fun getItemCount() = rates.size

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.bindCurrency(
            rates[position].first,
            rates[position].second,
            baseAmount,
            clickListener
        )
    }

    class CurrencyViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        fun bindCurrency(
            currencyIsoCode: String,
            currencyRate: Double,
            baseAmount: Double,
            clickListener: (String, Double) -> Unit
        ) {
            view.currencyIsoCode.text = currencyIsoCode
            view.currencyInput.setText(
                if (baseAmount == 0.0)
                    ""
                else
                    view.resources.getString(R.string.rate_format,baseAmount * currencyRate)
            )
            val currency = Currency.getInstance(currencyIsoCode)
            view.currencyName.text = currency.displayName

            view.itemHolder.setOnClickListener { clickListener(currencyIsoCode, baseAmount * currencyRate) }

            Glide.with(view)
                .load(
                    view.resources.getString(
                        R.string.currency_flag_api_url,
                        currencyIsoCode.toLowerCase()
                    )
                )
                .circleCrop()
                .into(view.currencyFlag)
        }
    }
}