package com.danielmartinak.revolut.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.danielmartinak.revolut.communication.CurrencyRateServiceController
import com.danielmartinak.revolut.communication.RevolutAPI
import com.danielmartinak.revolut.communication.model.CurrencyRates
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import java.util.concurrent.TimeUnit


class CurrencyRateRepository {
    private val INITIAL_DELAY = 0L
    private val PERIOD = 1L

    private var revolutAPI: RevolutAPI = CurrencyRateServiceController().revolutAPI
    private var disposable = CompositeDisposable()

    companion object {
        private val currencyRateRepository = CurrencyRateRepository()

        fun getNewInstance(): CurrencyRateRepository {
            return currencyRateRepository
        }
    }

    fun getCurrencyRatesByBaseCurrency(
        base: String,
        currencyRates: MutableLiveData<CurrencyRates>
    ) {

        disposable.add(Observable.interval(INITIAL_DELAY, PERIOD, TimeUnit.SECONDS)
            .flatMap { revolutAPI.getCurrencyRatesByBase(base) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result -> currencyRates.value = result },
                { throwable -> Log.e("Error", throwable.toString()) }
            ))
    }

    fun reset() {
        disposable.clear()
    }
}