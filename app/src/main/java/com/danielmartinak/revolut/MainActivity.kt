package com.danielmartinak.revolut

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.danielmartinak.revolut.recyclerview.CurrencyRecyclerAdapter
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity() {

    private lateinit var currencyViewModel: CurrencyViewModel
    private lateinit var currencyRecyclerAdapter: CurrencyRecyclerAdapter

    private lateinit var inputMethodManager: InputMethodManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        inputMethodManager= getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        currencyViewModel = ViewModelProviders.of(this)[CurrencyViewModel::class.java]

        currencyRecyclerAdapter = CurrencyRecyclerAdapter(mutableListOf())
        { baseCurrency: String, baseAmount: Double ->
            currencyViewModel.setBaseCurrency(baseCurrency)
            currencyViewModel.setBaseAmount(baseAmount)
            updateBaseItemHolder()
        }
        currencyRecyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = currencyRecyclerAdapter
        }

        currencyViewModel.currencyRates.observe(this, Observer { currencyRates ->
            currencyRates?.let {
                currencyRecyclerAdapter.rates = currencyRates.rates.toList()
                currencyRecyclerAdapter.notifyDataSetChanged()
            }
        })

        currencyViewModel.baseAmount.observe(this, Observer { baseAmount ->
            currencyRecyclerAdapter.baseAmount = baseAmount
            currencyRecyclerAdapter.notifyDataSetChanged()
        })

        updateBaseItemHolder()
        baseCurrencyInput.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(text: Editable?) {
            }

            override fun beforeTextChanged(text: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {
                currencyViewModel.setBaseAmount(
                    if (text.toString() == "")
                        0.0
                    else
                        text.toString().toDouble()
                )
            }
        })
    }

    private fun updateBaseItemHolder() {
        baseCurrencyIsoCode.text = currencyViewModel.baseCurrency.value
        baseCurrencyName.text =
            Currency.getInstance(currencyViewModel.baseCurrency.value).displayName
        baseCurrencyInput.apply {
            setText(
            if (currencyViewModel.baseAmount.value == 0.0)
                ""
            else
                getString(R.string.rate_format, currencyViewModel.baseAmount.value))
            requestFocus()
            baseCurrencyInput.setSelection(text.length)
            inputMethodManager.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
        }
        Glide.with(this)
            .load(
                getString(
                    R.string.currency_flag_api_url,
                    currencyViewModel.baseCurrency.value!!.toLowerCase()
                )
            )
            .circleCrop()
            .into(baseCurrencyFlag)
    }

    override fun onResume() {
        super.onResume()
        currencyViewModel.startCurrencyPolling()
    }

    override fun onPause() {
        super.onPause()
        currencyViewModel.stopCurrencyPolling()
    }
}
