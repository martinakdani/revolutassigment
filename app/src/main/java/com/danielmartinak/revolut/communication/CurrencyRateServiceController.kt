package com.danielmartinak.revolut.communication

import com.danielmartinak.revolut.BuildConfig
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class CurrencyRateServiceController {

    private val API_URL = BuildConfig.CURRENCY_API_URL

    var revolutAPI: RevolutAPI

    init {
        val gson = GsonBuilder().setLenient().create()

        val retrofitBuilder = Retrofit.Builder()
            .baseUrl(API_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())

        if (BuildConfig.BUILD_TYPE == "debug") {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder().addInterceptor(loggingInterceptor).build()
            retrofitBuilder.client(client)
        }
        val retrofit = retrofitBuilder.build()

        revolutAPI = retrofit.create(RevolutAPI::class.java)
    }
}