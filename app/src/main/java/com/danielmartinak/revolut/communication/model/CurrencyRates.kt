package com.danielmartinak.revolut.communication.model

import java.util.*

data class CurrencyRates(
    val base: String,
    val date: Date,
    val rates: HashMap<String, Double>
)