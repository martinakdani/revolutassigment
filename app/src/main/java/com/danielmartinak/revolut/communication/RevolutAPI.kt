package com.danielmartinak.revolut.communication

import com.danielmartinak.revolut.communication.model.CurrencyRates
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface RevolutAPI {

    @GET("/latest?")
    fun getCurrencyRatesByBase(@Query("base") base: String): Observable<CurrencyRates>
}