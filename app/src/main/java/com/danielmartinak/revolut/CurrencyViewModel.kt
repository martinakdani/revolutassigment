package com.danielmartinak.revolut

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.danielmartinak.revolut.communication.model.CurrencyRates
import com.danielmartinak.revolut.repository.CurrencyRateRepository

class CurrencyViewModel : ViewModel() {

    private var _baseCurrency = MutableLiveData<String>()
    val baseCurrency: LiveData<String>
        get() = _baseCurrency

    private var _baseAmount = MutableLiveData<Double>()
    val baseAmount: LiveData<Double>
        get() = _baseAmount

    private var _currencyRates = MutableLiveData<CurrencyRates>()
    val currencyRates: LiveData<CurrencyRates>
        get() = _currencyRates

    private val currencyRateRepository = CurrencyRateRepository.getNewInstance()

    init {
        _baseCurrency.value = "EUR"
        _baseAmount.value = 1.0
    }

    fun setBaseCurrency(baseCurrency: String) {
        _baseCurrency.value = baseCurrency
        stopCurrencyPolling()
        startCurrencyPolling()
    }

    fun setBaseAmount(amount: Double) {
        _baseAmount.value = amount
    }

    fun startCurrencyPolling() {
        currencyRateRepository.getCurrencyRatesByBaseCurrency(_baseCurrency.value!!, _currencyRates)
    }

    fun stopCurrencyPolling() {
        currencyRateRepository.reset()
    }
}